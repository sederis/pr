<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('project.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  validation
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        $project = new \App\Project;
        $project->title = $request->title;
        $project->description = $request->description;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->creation_date = \Carbon\Carbon::now()->format('d.m.Y');
        $project->author_id = \Auth::user()->id;
        $project->save();
        //  IMPORTANT - first save, else there is no project-id to attach our users to!!
        $project->allWorkers()->attach($request->employee);

        return redirect('project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = \App\Project::find($id);
        return view('project.single')->with('project', $project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = \App\Project::find($id);
        return view('project.edit')->with('project', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  validation
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
        ]);

        $project = \App\Project::find($id);
        $project->title = $request->title;
        $project->description = $request->description;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->creation_date = \Carbon\Carbon::now()->format('d.m.Y');
        $project->author_id = \Auth::user()->id;
        $project->save();
        //  IMPORTANT - first save, else there is no project-id to attach our users to!!
        $project->allWorkers()->detach();
        $project->allWorkers()->attach($request->employee);

        $project->save();
        return redirect('project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = \App\Project::find($id);
        $project->allWorkers()->detach();
        $project->allMilestones()->detach();
        $project->allTasks()->detach();
        $project->delete();
        return redirect('project');
    }
}

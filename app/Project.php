<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	public $timestamps = false;
	
    public function allWorkers() {
    	return $this->belongsToMany('App\User', 'rel_projects_users', 'project_id', 'user_id');
    }

    public function allTasks() {
    	return $this->hasMany('App\Task');
    }

    public function allMilestones() {
    	return $this->hasMany('App\Milestone', 'rel_projects_milestones', 'project_id', 'milestone_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $timestamps = false;

    public function parentProject() {
    	return $this->belongsTo('App\Project', 'project_id', 'id');
    }

}

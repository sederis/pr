@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Project Create</div>
                 <div class="form-group">
                    <form action="../../project/{{$project->id}}" method="post" id="createform">
                        <input name="_method" type="hidden" value="PUT">
                        <input type="text" placeholder="Titel" id="title" name="title" value="{{$project->title}}">
                        <input type="text" placeholder="Startdatum" id="start_date" data-provide="datepicker" name="start_date" value="{{$project->start_date}}">
                        <input type="text" placeholder="Enddatum" id="end_date" data-provide="datepicker" name="end_date" value="{{$project->end_date}}">
                        <textarea type="text" rows="10" name="description" placeholder="Beschreibung">{{$project->description}}</textarea>
                        {{ csrf_field() }}
                        @foreach(\App\User::all() as $user)
                            <span><input type="checkbox" name="employee[]" value="{{$user->id}}"@if(\App\Project::find($project->id)->allWorkers()->get()->contains($user->id)) checked @endif>{{$user->name}}</span>
                        @endforeach
                        <button type="submit">Speichern</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">

    form {
        width: 50%;
        margin: auto;
    }

    form * {
        width: 100%;
        margin-top: 10px !important;
        margin-bottom: 10px !important;
    }

    #similar {
        position: absolute;
        left: 0px;
        bottom: 0px;
        width: 400px;
        height: 750px;
        background-color: darkgrey;
    }
</style>
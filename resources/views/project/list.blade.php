@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Projektliste<span class="pull-right"><a href="project/create"><i class="fa fa-calendar-plus-o"></i> erstellen</a></span></div>
                <div class="panel-body">
                    @foreach(\App\Project::all() as $project)
                    <a href="project/{{$project->id}}">{{$project->title}}</a></br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
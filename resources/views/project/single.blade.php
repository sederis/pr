@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="panel-heading">{{$project->title}}<span class="pull-right">
        <form class="pull-right" action="{{ route('project.destroy', $project->id) }}" method="POST">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button><i class="fa fa-trash-o"></i> löschen</button>
        </form> 
        <a href="../task/create/{{$project->id}}"><i class="fa fa-plus"></i>aufgabe hinzufügen</a> <a href="../project/{{$project->id}}/edit"><i class="fa fa-pencil"></i>projekt editieren</a>
    </span></div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                	<p>Name: {{$project->title}}</p>
                	<p>Beschreibung: {{$project->description}}</p>
                	<p>Start: {{$project->start_date}}</p>
                	<p>Ende: {{$project->end_date}}</p>
                	<p>Mitarbeiter:</p>
                    <p>
                	@foreach($project->allWorkers()->get() as $worker)
                		<a href="../user/{{$worker->id}}">{{$worker->name}}</a></br>
                	@endforeach
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Aufgaben:</p>
                    @foreach($project->allTasks()->get() as $task)
                        <p><a class="focus" data-id="{{$task->id}}" title="Auf Timeline fokussieren"><i class="fa fa-eye"></i></a> <a href="../task/{{$task->id}}">{{$task->title}}</a></p>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Name: {{$project->title}}</p>
                    <p>Beschreibung: {{$project->description}}</p>
                    <p>Start: {{$project->start_date}}</p>
                    <p>Ende: {{$project->end_date}}</p>
                    <p>Mitarbeiter:</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="timeline_controls">
    <a id="fit">[fit timeline]</a>
</div>
<div id="timeline"></div>
@endsection
@section('page-js-script')
<script type="text/javascript">
    jQuery(document).ready(function(){
        var items = new vis.DataSet([
            @foreach($project->allTasks()->get() as $task)
            {id: {{$task->id}}, content: '{{$task->title}}', start: new Date("{{$task->start_date}}".split(".")[2], "{{$task->start_date}}".split(".")[1]-1, "{{$task->start_date}}".split(".")[0]), end: new Date("{{$task->end_date}}".split(".")[2], "{{$task->end_date}}".split(".")[1]-1, "{{$task->end_date}}".split(".")[0]), title: "<p>{{$task->description}}</p><p>Start: {{$task->start_date}}</p><p>Ende: {{$task->end_date}}</p><p>Sachbearbeiter:{{\App\User::find($task->user_id)->name}}</p>" },
            @endforeach
            {id: 61, content: 'Milestone', start: '2017-11-27', type: 'point'}
        ]);
        var container = document.getElementById("timeline")
        var options = {
            height: "300px",
            locale: "de",
            editable: {
                add: true,
                updateTime: true,
                overrideItems: true,
                updateGroup: true
            },
            //  Enable Tooltips
            tooltip: {
                followMouse: true,
                overflowMethod: 'cap'
            },
            //  Snap to full days
            snap: function(date, scale, step) {
                var clone = new Date(date.valueOf());
                scale = 'day';
                step = 2;
                return vis.timeline.TimeStep.snap(clone, scale, step);
            },
            onMove: function(item, callback) {
                var d = new Date(item.start);
            }
        };
        var timeline = new vis.Timeline(container, items, options);

        jQuery("#fit").click(function() {
            timeline.fit();
        });
        jQuery(".focus").click(function() {
            timeline.focus(jQuery(this).data("id"));
        });
    });
</script>
@endsection
<style type="text/css">
    table {
        width: 100%;
    }
    table tr td {
        padding: 5px;
        border: 1px solid rgba(0, 0, 0, .1);
    }
    button {
        background:none;
        border:none;
        margin:0;
        padding:0;
        color: #3097D1 !important;
        cursor: pointer;
    }

    button:hover {
        text-decoration: underline;
        color: #216a94 !important;
    }
</style>
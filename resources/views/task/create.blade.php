@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Task Create</div>
                 <div class="form-group">
                    <form action="../../task" method="post" id="createform">
                        <input type="text" placeholder="Titel" id="title" name="title">
                        <input type="text" placeholder="Startdatum" id="start_date" data-provide="datepicker" name="start_date">
                        <input type="text" placeholder="Enddatum" id="end_date" data-provide="datepicker" name="end_date">
                        <input type="text" placeholder="Budget an Mannstunden" id="budget" name="budget">
                        <input type="hidden" name="projectid" value="{{$projectid}}">
                        <textarea type="text" rows="10" name="description" placeholder="Beschreibung"></textarea>
                        {{ csrf_field() }}
                        <select name="employee" >
                        @foreach(\App\Project::find($projectid)->allWorkers()->get() as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                        </select>
                        <button type="submit">Speichern</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">

    form {
        width: 50%;
        margin: auto;
    }

    form * {
        width: 100%;
        margin-top: 10px !important;
        margin-bottom: 10px !important;
    }

    #similar {
        position: absolute;
        left: 0px;
        bottom: 0px;
        width: 400px;
        height: 750px;
        background-color: darkgrey;
    }
</style>
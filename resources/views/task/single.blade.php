@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Task Create<span class="pull-right"><a href="../project/{{$task->project_id}}">[back to project]</a></span></div>
                 <div class="form-group">
                    <p>Überschrift: {{$task->title}}</p>
                    <p>Anfangsdatum: {{$task->start_date}}</p>
                    <p>Enddatum: {{$task->end_date}}</p>
                    <p>Budget an Mannstunden: {{$task->budget}}</p>
                    <p>Fertigstellung: {{$task->completion/$task->budget}}</p>
                    <p>Beschreibung: {{$task->description}}</p>
                    {{ csrf_field() }}
                    <p>Zuständig: {{\App\User::find($task->user_id)->name}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">

    form {
        width: 50%;
        margin: auto;
    }

    form * {
        width: 100%;
        margin-top: 10px !important;
        margin-bottom: 10px !important;
    }

    #similar {
        position: absolute;
        left: 0px;
        bottom: 0px;
        width: 400px;
        height: 750px;
        background-color: darkgrey;
    }
</style>
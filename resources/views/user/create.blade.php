@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Project Create</div>
                 <div class="form-group">
                    <form action="../user" method="post" id="createform">
                        <input type="text" placeholder="Name" id="name" name="name">
                        <input type="text" placeholder="E-Mail" id="email" name="email">
                        <input type="text" placeholder="Passwort" id="passwort" name="passwort">
                        {{ csrf_field() }}
                        <button type="submit">Speichern</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">

    form {
        width: 50%;
        margin: auto;
    }

    form * {
        width: 100%;
        margin-top: 10px !important;
        margin-bottom: 10px !important;
    }

    #similar {
        position: absolute;
        left: 0px;
        bottom: 0px;
        width: 400px;
        height: 750px;
        background-color: darkgrey;
    }
</style>
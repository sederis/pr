@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User List<span class="pull-right"><a href="user/create">[create]</a></span></div>
                <div class="panel-body">
                    @foreach(\App\User::all() as $user)
                    <span><a href="user/{{$user->id}}">{{$user->name}}</a></span><br/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
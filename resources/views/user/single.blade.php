@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            	<div class="panel-heading">
            		<form class="pull-right" action="{{ route('user.destroy', $user->id) }}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button>[delete]</button>
                </form>
            	</div>
                <div class="panel-body">{{$user->name}}</div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
    table {
        width: 100%;
    }
    table tr td {
        padding: 5px;
        border: 1px solid rgba(0, 0, 0, .1);
    }
    button {
        background:none;
        border:none;
        margin:0;
        padding:0;
        color: #3097D1 !important;
        cursor: pointer;
    }

    button:hover {
        text-decoration: underline;
        color: #216a94 !important;
    }
</style>